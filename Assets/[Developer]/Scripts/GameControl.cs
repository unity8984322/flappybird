using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;

    public GameObject GameOverText;
    public Text scoreText;
    public int birdScore;
    public float scrollSpeed = -1.5f;
    public bool gameOver;
   

    //Make this class as singleton
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
            Destroy(gameObject);
    }

    private void Update()
    {
        //if(gameover==true and mouce input given)
        if(gameOver==true && Input.GetMouseButtonDown(0))
        {
            //Reload Scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void BirdDied()
    {
        //After bird died
        gameOver = true;
        GameOverText.SetActive(true);
    }

    public void BirdScore()
    {
        //Show Score on the UI
        if (gameOver)
        {
            return;
        }
        birdScore++;

        //if (birdScore % 10 == 0)
        //{
          //  Debug.Log("Scrolling speed increased");
            //RepeatingBackground.instance.scrollSpeed -= 0.5f;
        //}

        scoreText.text = "Score: "+birdScore.ToString();
    }
}
