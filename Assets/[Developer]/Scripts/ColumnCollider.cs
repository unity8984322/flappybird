using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<BirdController>() != null){
            GameControl.instance.BirdScore();
            if (GameControl.instance.birdScore % 10 == 0)
            {
                Debug.Log("Scrolling speed increased");
                RepeatingBackground.instance.scrollSpeed -= 0.5f;
            }
        }
    }
}
