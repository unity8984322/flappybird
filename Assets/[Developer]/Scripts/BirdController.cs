using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    private Rigidbody2D birdRb;
    private Animator anim;

    private bool isDead;
    public float forceAmount=10f;    
    void Start()
    {
        birdRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    } 

    void Update()
    {
        Camera.main.transform.position = new Vector3(birdRb.transform.position.x+1, 0, -10);

        //if bird is not dead then do the following
        if (!isDead)
        {
            //If mouce input is given then add upward force 
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                birdRb.velocity = Vector2.zero;                         //This will make bird's velocity zero
                birdRb.AddForce(forceAmount * new Vector2(0, forceAmount), ForceMode2D.Impulse);
                anim.SetTrigger("Flap");                                //Play animation                          
            }
        }
    }

    private void OnCollisionEnter2D()
    {
        birdRb.velocity = Vector2.zero;                             //stop bird movements
        isDead = true;                                              //if collided then isDead==true
        GameControl.instance.BirdDied();                            //call method BirdDied()
    }
}
