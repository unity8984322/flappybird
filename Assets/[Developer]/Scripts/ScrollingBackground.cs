using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public static ScrollingBackground instance;
    [HideInInspector] public Rigidbody2D scrollingBodyRB;
    //Get the rigidbody of backgruond
    void Start()
    {
        scrollingBodyRB = gameObject.GetComponent<Rigidbody2D>();
        //below statement will move our sscrolingBodyRB backwards (-x axis) with scrollSpeed
        scrollingBodyRB.velocity = new Vector2(GameControl.instance.scrollSpeed, 0);
    }
        
    void Update() 
    {
        //Stop the scrolling body when game is over
        if (GameControl.instance.gameOver)
        {
            scrollingBodyRB.velocity = Vector2.zero;
        }    
    }
}
