using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour
{
    private GameObject[] columns;       //Columns gameobjects that you want to instantiate
    public GameObject columnPrefab;     //Column pRefab to instantiat columns

    private Vector2 objectPoolPos = new Vector2(-15f,-25f);

    public int columnPoolSize = 5;          //Total number of columns to be instantiated
    private float timeSinceLastSpawned;     //Time after which colums must be repeated
    public float spawnRate = 4f;            
    public float columnYMin = -1f;          
    public float columnYMax = 3.5f;
    private float spawnXPos = 10f;
    private int currentColumn = 0;
    
    void Start()
    {
        columns = new GameObject[columnPoolSize];
        for(int i = 0; i < columnPoolSize; i++)
        {
            columns[i] = (GameObject) Instantiate(columnPrefab, objectPoolPos, Quaternion.identity);    //Instantiate column as gameobject
        }
    }

    void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;
        if(GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0;
            float spawnYpos = Random.Range(columnYMin, columnYMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPos, spawnYpos);
            currentColumn++;
            if (currentColumn >= 5)
            {
                currentColumn = 0;
            }
        }
    }
}
